# Michael Stiven Osorio Riaño 
## Projects
 - [Michael Osorio](https://gitlab.com/MichaelOsorio2017/isis3510_201920_p_ms.osorio)
 - [My Bluetooth Application](https://gitlab.com/MichaelOsorio2017/mybluetoothapplication)
## Wiki
 - [Expectations and experiences](https://gitlab.com/MichaelOsorio2017/isis3510_201920_p_ms.osorio/wikis/Expectations-and-experience)
 - [MS1 O1](https://gitlab.com/MichaelOsorio2017/isis3510_201920_p_ms.osorio/wikis/MS1-O1)
 - [MS1](https://gitlab.com/MichaelOsorio2017/isis3510_201920_p_ms.osorio/wikis/MS1)
 - [MS2 O1](https://gitlab.com/MichaelOsorio2017/isis3510_201920_p_ms.osorio/wikis/MS2-O1)
 - [MS2 O2](https://gitlab.com/MichaelOsorio2017/isis3510_201920_p_ms.osorio/wikis/MS2-O2)
 - [MS2 O3](https://gitlab.com/MichaelOsorio2017/isis3510_201920_p_ms.osorio/wikis/MS2%20O3?)
 - [MS2](https://gitlab.com/MichaelOsorio2017/isis3510_201920_p_ms.osorio/wikis/MS2)
## Issues
 - [List](https://gitlab.com/MichaelOsorio2017/isis3510_201920_p_ms.osorio/issues)
 - [Boards](https://gitlab.com/MichaelOsorio2017/isis3510_201920_p_ms.osorio/-/boards)